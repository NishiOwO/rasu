#ifndef __RASU_AUTH_H__
#include <stdio.h>
#include "../include/types.h"
struct rasu_config {
	bool no_keep_env;
	bool need_passwd;
	bool auth_success;
};
struct rasu_config rasu_auth();
#endif
#define __RASU_AUTH_H__
