#ifndef __RASU_UTIL_H__
#include <stdio.h>
#include "../include/types.h"
void rasu_help(FILE*, char*);
void rasu_version(FILE*);
bool rasu_file_exist(char*);
#endif
#define __RASU_UTIL_H__
