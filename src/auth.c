#include "../include/auth.h"
#include "../include/types.h"
#include "../include/util.h"
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#ifdef __linux__
#include <shadow.h>
#include <crypt.h>
#endif

struct rasu_config rasu_auth(){
	struct rasu_config config;
	config.no_keep_env = true;
	config.need_passwd = true;
	config.auth_success = false;
	if(!rasu_file_exist("/etc/rasu.d")){
		fprintf(stderr, "rasu: /etc/rasu.d doesn't exist. Configure rasu first.\n");
		exit(1);
	}else{
		struct passwd* p = getpwuid(getuid());
		char* str;
		str = (char*)malloc(sizeof(char) * (
			strlen("/etc/rasu.d/")
		+	strlen(p->pw_name)
		) + 2);
		memcpy(str, "/etc/rasu.d/", strlen("/etc/rasu.d/"));
		memcpy(str + strlen("/etc/rasu.d/"), p->pw_name, strlen(p->pw_name));
		str[strlen("/etc/rasu.d/") + strlen(p->pw_name)] = 0;
		if(!rasu_file_exist(str)){
			free(str);
			fprintf(stderr, "rasu: rasu hasn't configured for you (yet).\n");
			fprintf(stderr, "rasu: Tell the adminstrator to configure it for you.\n");
			exit(1);
		}else{
			FILE* file;
			char* line = (char*)malloc(1);
			line[0] = 0;
			int position = 1;
			if((file = fopen(str, "r"))){
				char c = 0;
				while(true){
					c = getc(file);
					if(c == EOF) break;
					char* tmp;
					if((tmp = (char*)realloc(line, position + 1)) == NULL){
						free(line);
						exit(1);
					}else{
						line = tmp;
					}
					if(c == '\n'){
						if(strcmp(line, "keepenv") == 0){
							config.no_keep_env = false;
						}else if(strcmp(line, "nopass") == 0){
							config.need_passwd = false;
						}
						free(line);
						line = (char*)malloc(1);
						position = 1;
					}else{
						line[position - 1] = c;
						line[position] = 0;
						position++;
					}
				}
				fclose(file);
			}
			free(str);
			free(line);
		}
		if(!config.need_passwd){
			config.auth_success = true;
			return config;
		}else{
			fprintf(stdout, "[rasu] Password for %s: ", p->pw_name);
			fflush(stdout);
			struct termios oldt;
			tcgetattr(STDIN_FILENO, &oldt);
			struct termios newt = oldt;
			newt.c_lflag &= ~ECHO;
			tcsetattr(STDIN_FILENO, TCSANOW, &newt);
			char* passwd = (char*)malloc(512);
			memset(passwd, 0, 512);
			scanf("%s", passwd);
			tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
			fprintf(stdout, "\n");
			int uid = getuid();
			p = getpwuid(uid);
			char* _passwd = p->pw_passwd;
#ifdef __linux__
			struct spwd* _spwd;
			if(strcmp(_passwd, "x") == 0){
				_spwd = getspnam(p->pw_name);
				_passwd = _spwd->sp_pwdp;
			}
#endif
			if(strcmp(crypt(passwd, _passwd), _passwd) != 0){
				free(passwd);
				return config;
			}
			free(passwd);
			config.auth_success = true;
		}
	}
	return config;
}
