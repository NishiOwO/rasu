#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <pwd.h>
#include <syslog.h>
#include "../include/types.h"
#include "../include/util.h"
#include "../include/auth.h"

int main(int argc, char** argv){
	if(argc <= 1){
		fprintf(stderr, "rasu: More than 1 arguments are required.\n");
		return 1;
	}
	struct passwd* passwd_current = getpwuid(getuid());
	bool is_still_option = true;
	bool shell = false;
	char** command_args;
	int count = 1;
	for(int arg_count = 1; arg_count < argc; arg_count++){
		char* str = argv[arg_count];
		if(str[0] == '-' && is_still_option){
			if(
				strcmp(str, "--help") == 0
			||	strcmp(str, "-h") == 0
			){
				rasu_help(stdout, argv[0]);
				return 0;
			}else if(
				strcmp(str, "--version") == 0
			||	strcmp(str, "-V") == 0
			){
				rasu_version(stdout);
				return 0;
			}else if(
				strcmp(str, "--shell") == 0
			||	strcmp(str, "-s") == 0
				){
				shell = true;
			}else{
				fprintf(stderr, "Unknown option: %s\n", str);
				return 1;
			}
		}else{
			if(is_still_option){
				command_args = (char**)malloc(sizeof(char*) * 2);
			}
			command_args[count - 1] = str;
			command_args[count - 0] = NULL;
			char** tmp;
			count++;
			if((tmp = realloc(command_args, sizeof(char*) * (count + 1))) == NULL){
				free(command_args);
			}else{
				command_args = tmp;
			}
			is_still_option = false;
		}
	}
	struct rasu_config result = rasu_auth();
	if(!result.auth_success){
		const char* before = "Failed authentication for ";
		char* str = (char*)malloc(strlen(before) + strlen(passwd_current->pw_name));
		strcpy(str, before);
		strcpy(str + strlen(before), passwd_current->pw_name);
		syslog(LOG_AUTHPRIV, str);
		fprintf(stderr, "Sorry, authentication failed.\n");
		return 1;
	}
	setuid(0);
	seteuid(0);
	setegid(0);
	setgid(0);
	if(getuid() != 0){
		fprintf(stderr, "panic!: Rasu has panicked. See authlog for more details.\n");
		syslog(LOG_AUTHPRIV|LOG_ERR, "getuid() was not zero. Is rasu owned by root, and does it have setuid permission?");
		return -1;
	}
	{
		const char* after = " ran:";
		char* str = (char*)malloc(strlen(after) + strlen(passwd_current->pw_name));
		strcpy(str, passwd_current->pw_name);
		strcpy(str + strlen(passwd_current->pw_name), after);
		char* result;
		asprintf(&result, "%s", str);
		for(int i = 1; i < argc; i++){
			asprintf(&result, "%s %s", result, argv[i]);
		}
		syslog(LOG_AUTHPRIV, result);
	}
	if(shell){
		struct passwd* p = getpwuid(0);
		char** args = (char**)malloc(sizeof(char*) * 2);
		args[0] = (char*)malloc(strlen(p->pw_shell));
		memcpy(args[0], p->pw_shell, strlen(p->pw_shell));
		args[1] = NULL;
		if(result.no_keep_env){
			execvpe(args[0], args, NULL);
		}else{
			execvp(args[0], args);
		}
	}else{
		if(result.no_keep_env){
			execvpe(command_args[0], command_args, NULL);
		}else{
			execvp(command_args[0], command_args);
		}
		fprintf(stderr, "rasu: %s: %s\n", command_args[0], strerror(errno));
		exit(1);
	}
}
