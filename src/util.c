#include <stdio.h>
#include <unistd.h>
#include "../include/types.h"
#include "../include/util.h"
#include "../include/rasu.h"

void rasu_help(FILE* fp, char* command){
	rasu_version(fp);
	fprintf(fp, "Usage: %s [--help|-h|--version|-V]\n",
		command
	);
	fprintf(fp, "       %s [--shell|-s]\n",
		command
	);
	fprintf(fp, "       %s [command]\n",
		command
	);
}
void rasu_version(FILE* fp){
	fprintf(fp, "rasu - Run As Super User version %s\n",
			RASU_VERSION
	);
	fprintf(fp, "Copyright (C) 2022 NishiOwO. All rights reserved.\n");
	fprintf(fp, "rasu is licensed under the 3-clause BSD License.\n");
}
bool rasu_file_exist(char* filename){
	return access(filename, F_OK) == 0;
}
